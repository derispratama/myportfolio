import React, { Component } from 'react';
import './hero.css';
class Hero extends React.Component {
    render() {
        return (
            <div className="hero">
                <div className="hero-wrapper">
                    <div className="foto md:bg-contain" style={{ backgroundImage: `url("assets/img/deris.jpg")` }}></div>
                    <div className="hero-text">
                        Deris Ganesha Pratama
                    </div>
                    <div className="garis flex">
                        <div className="w-72 h-1.5 mt-5 bg-light rounded-lg"></div>
                        <div className="w-1.5 h-1.5 mt-5 rounded-full mx-2 bg-light"></div>
                        <div className="w-72 h-1.5 mt-5 bg-light rounded-lg"></div>
                    </div>
                    <div className="mt-2 text-light text-xl font-semibold">Full Stack Developer</div>
                </div>
            </div>
        );
    }
}

export default Hero;