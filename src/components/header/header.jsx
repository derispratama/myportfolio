import React, { Component } from 'react';
import './header.css';

class Header extends React.Component {
    render() {
        return (
            <div className="header">
                <div className="header-wrapper">
                    <div className="logo hidden lg:block">Deris Ganesha Pratama</div>
                    <div className="header-link">
                        <div className="border-b-2 border-transparent hover:border-brand transform duration-300">Skills</div>
                        <div className="border-b-2 border-transparent hover:border-brand transform duration-300">Portfolio</div>
                        <div className="border-b-2 border-transparent hover:border-brand transform duration-300">Contact</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;