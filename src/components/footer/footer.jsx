import React, { Component } from 'react';
import './footer.css';
class Footer extends React.Component {
    render() {
        return (
            <div className="footer text-center p-5 bg-dark text-light">
                <span>Copyright © 2021 Deris Ganesha Pratama. All rights reserved.</span>
            </div>
        );
    }
}

export default Footer;