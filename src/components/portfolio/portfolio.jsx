import React, { Component } from 'react';
import './portfolio.css';
class Portfolio extends React.Component {
    render() {
        return (
            <div className="portfolio bg-cyans">
                <div className="header-portfolio flex flex-col justify-center items-center bg-cyans">
                    <div className="title">PORTFOLIO</div>
                    <div className="flex">
                        <div className="w-32 h-1.5 mt-5 bg-light rounded-lg"></div>
                        <div className="w-1.5 h-1.5 mt-5 rounded-full mx-2 bg-light"></div>
                        <div className="w-32 h-1.5 mt-5 bg-light rounded-lg"></div>
                    </div>
                </div>
                <div className="container px-5 py-24 mx-auto">
                    <div className="flex flex-wrap -m-4">
                        <div className="w-full lg:w-1/3 sm:w-1/2 p-4">
                            <div className="flex relative h-52">
                                <img alt="gallery" className="absolute inset-0 w-full h-full object-cover object-center" src="assets/img/p1.png" />
                                <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                                </div>
                            </div>
                        </div>
                        <div className="w-full lg:w-1/3 sm:w-1/2 p-4">
                            <div className="flex relative h-52">
                                <img alt="gallery" className="absolute inset-0 w-full h-full object-cover object-center" src="assets/img/siptrunk.png" />
                                <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                                </div>
                            </div>
                        </div>
                        <div className="w-full lg:w-1/3 sm:w-1/2 p-4">
                            <div className="flex relative h-52">
                                <img alt="gallery" className="absolute inset-0 w-full h-full object-cover object-center" src="assets/img/rli.png" />
                                <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Portfolio;