import React, { Component } from 'react';
import Contact from './components/contact/contact';
import Footer from './components/footer/footer';
import Header from './components/header/header';
import Hero from './components/hero/hero';
import Portfolio from './components/portfolio/portfolio';
import Resume from './components/resume/resume';
import Skill from './components/skill/skill';

class App extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <div className="min-h-screen">
          <Hero />
          <Skill/>
          {/* <Resume/> */}
          <Portfolio/>
          <Contact/>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default App;