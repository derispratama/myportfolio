const colors = require('tailwindcss/colors');
module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      gray: colors.coolGray,
      blue: colors.lightBlue,
      red: colors.rose,
      pink: colors.fuchsia,
      primary: '#321fdb',
      secondary: '#ced2db',
      info: '#ced2db',
      danger: '#e55353',
      success: '#2eb85c',
      light: '#ffffff',
      dark: '#0C6980',
      cyans: '#00A8A8',
      darken: '#1a237e',
      brand: '#28cbe0',
      background: '#232427',
      transparent: '#00000000',
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
